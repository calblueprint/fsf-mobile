import React from 'react';
import {
  Button,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View
} from 'react-native';
import StepIndicator from 'react-native-step-indicator';

import AmountComponent from '../../components/donations/AmountComponent';
import BillingComponent from '../../components/donations/BillingComponent';
import PaymentComponent from '../../components/donations/PaymentComponent';
import BaseScreen from '../BaseScreen';
import colors from '../../styles/colors';
import {
  okAlert
} from '../../lib/alerts';
import {
  TCGetBillingID,
  TCSinglePayment,
  storeBillingID,
  storeLastFour,
  storeCardholder
} from '../../lib/donate';
import {
  getStoredApiKey,
  getStoredEmail
} from '../../lib/login';

import anonymousDonationSettings from '../../config/anonymous-donations';

const labels = ['Amount', 'Billing Information', 'Payment Information']
class DonateScreen extends BaseScreen {
  constructor(props) {
    super(props);
    this.state = {
      amount: '',
      savedCC: false,
      loadingCC: true,
      currentPosition: 0,
      address: '',
      city: '',
      country: '',
      stateProvince: '',
      postalCode: '',
      cardholder: '',
      cc: '',
      exp: '',
      securityCode: '',
      valid_cc: false,
    }
  }

  donate = async () => {
    try {
      var amount = this.state.amount.toString();

      // Represent amount in the number of cents
      // e.g. $10.34 = 1034
      // TODO: validation of the correctness of amount
      const dotIndex = amount.indexOf('.');
      if (dotIndex == -1) {
        amount = amount + '00';
      } else {
        amount = amount.substring(0, dotIndex) +
                 amount.substring(dotIndex + 1, dotIndex + 3);
      }

      /*
        ANONYMOUS DONATIONS:
          Current approach is to create an Anonymous_Payment User with:
            - email: anonymous@payment.com
            - apiKey: whatever api key CiviCRM returns, simply hardcore this apikey into all anonymous payments
          
            I didn't actually go ahead to make this user because I'm not sure if this is the approach we wanna take

            Taking this approach, we the rest of the function call both here
            and in the backend is unchanged because we simply pass in the email and apikey 
            of the correspending 'anonymous' user
      */

     let email = anonymousDonationSettings.ANONYMOUS_EMAIL;
     let apiKey = anonymousDonationSettings.ANONYMOUS_API_KEY;
      try {
        email = await getStoredEmail();
        apiKey = await getStoredApiKey(); 
      } catch (error) {
        console.log('anonymous donation');
      }

      var exp = this.state.exp;
      exp = exp.substring(0, 2) + exp.substring(3);

      var cc = this.state.cc;
      cc = cc.replace(/\s+/g, '');

      var cvv = this.state.securityCode;
      // Jason: need to check what cvv looks like and sanitize if necessary

      tcInfo = {
        'name': this.state.cardholder,
        'cc': cc,
        'exp': exp,
        'cvv': cvv,
        'amount': amount,
        'email': email,
        'apikey': apiKey
      };
      const transResp = await TCSinglePayment(tcInfo);

      if (transResp.status != 'approved') {
        okAlert('Error: Transaction not approved', 'Try again');
      } else {
        // TODO JASON: This is where tyou want to add the 'toggle' of whether the save the user's 
        // billing ID or not
        const resp = await TCGetBillingID(tcInfo);
        await storeBillingID(resp.billingid);
        await storeLastFour(tcInfo['cc'].slice(8, 12));
        await storeCardholder(this.state.cardholder)
        okAlert('Success! Transaction ID: ' + transResp.transid);
        this.props.navigation.navigate('DonateSuccess', {
          amount: this.state.amount.toString(),
          cardholder: this.state.cardholder,
        });
      }
    } catch(error) {
      okAlert('Donate failed', 'Try again');
      // Can considering uncommenting this line just to show them what the complete flow will look like
      // this.props.navigation.navigate('DonateSuccess');
    }
  };

  handleChange = (target, text) => {
    this.setState({ [target]: text });
  };

  handleCreditCardChange = (form) => {
    console.log(form)
    this.setState({
      'cc': form.values.number,
      'exp': form.values.expiry,
      'securityCode': form.values.cvc,
      'cardholder': form.values.name,
      'valid_cc': form.valid,
    });
  };

  onPageChange = (position) => {
    this.setState({ currentPosition: position })
  };

  renderStepComponent() {
    if (this.state.currentPosition == 0) {
      return (
        <AmountComponent
          handleChange={this.handleChange}
          changePage={this.onPageChange}
          styles={styles}
          amount={this.state.amount}
        />
      )
    } else if (this.state.currentPosition == 1) {
      return (
        <BillingComponent
          handleChange={this.handleChange}
          changePage={this.onPageChange}
          styles={styles}
          props={this.state}
        />
      )
    } else if (this.state.currentPosition == 2) {
      return (
        <PaymentComponent
          handleChange={this.handleCreditCardChange}
          changePage={this.onPageChange}
          styles={styles}
          amount={this.state.amount}
          disabledButton={this.state.valid_cc}
          donate={this.donate}
        />
      )
    }
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={{flex: 1}}>
          <KeyboardAvoidingView
            style={{ flex: 1, marginTop: 10 }}
            behavior={(Platform.OS === 'ios')? "padding" : null}
          >
            <StepIndicator
              currentPosition={this.state.currentPosition}
              labels={labels}
              stepCount={3}
              onPress={this.onPageChange}
            />
            {this.renderStepComponent()}
          </KeyboardAvoidingView>
        </View>
      </TouchableWithoutFeedback>

    );
  }
}

const styles = StyleSheet.create({
  donationButton: {
    marginTop: 20,
    backgroundColor: '#27ae60',
  },
  disabledDonationButton: {
    marginTop: 20,
    backgroundColor: '#27ae60',
    opacity: .25
  },
  donationButtonContent: {
    height: 50
  },
  donationButtonText: {
    color: colors.textLight,
    fontSize: 18
  },
  container: {
    flex: 1,
    marginLeft: 12,
    marginRight: 12,
    marginTop: 15
  }
});

export default DonateScreen;
